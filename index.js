const { server, app } = require('./generateServer')();
const mongoose = require('mongoose');
const videowikiGenerators = require('@videowiki/generators');
const DB_CONNECTION = process.env.SOCKET_CONNECTION_SERVICE_DATABASE_URL;
let mongoConnection 
mongoose.connect(DB_CONNECTION)
.then((con) => {
    mongoConnection = con.connection;
    con.connection.on('disconnected', () => {
        console.log('Database disconnected! shutting down service')
        process.exit(1);
    })

    videowikiGenerators.healthcheckRouteGenerator({ router: app, mongoConnection });

    const SocketConnection = require('./models').SocketConnection;

    app.get('/count', (req, res) => {
        console.log('hello count')
        SocketConnection.count(req.query)
        .then(count => res.json(count))
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message)
        })

    })



    // CRUD routes
    app.get('/', (req, res) => {
        const { sort, skip, limit, one, ...rest} = req.query;
        let q;
        if (!rest || Object.keys(rest || {}).length === 0) {
            return res.json([]);
        }

        Object.keys(rest).forEach((key) => {
            if (key.indexOf('$') === 0) {
                const val = rest[key];
                rest[key] = [];
                Object.keys(val).forEach((subKey) => {
                    val[subKey].forEach(subVal => {
                        rest[key].push({ [subKey]: subVal })
                    })
                })
            }
        })
        if (one) {
            q = SocketConnection.findOne(rest);
        } else {
            q = SocketConnection.find(rest);
        }
        if (sort) {
            Object.keys(sort).forEach(key => {
                q.sort({ [key]: parseInt(sort[key]) })
            })
        }
        if (skip) {
            q.skip(parseInt(skip))
        }
        if (limit) {
            q.limit(parseInt(limit))
        }
        q.then((socketConnections) => {
            return res.json(socketConnections);
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    })

    app.post('/', (req, res) => {
        const data = req.body;
        SocketConnection.create(data)
        .then((socketConnection) => {
            return res.json(socketConnection);
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    })

    app.patch('/', (req, res) => {
        let { conditions, values, options } = req.body;
        if (!options) {
            options = {};
        }
        SocketConnection.update(conditions, { $set: values }, { ...options, multi: true })
        .then(() => SocketConnection.find(conditions))
        .then(socketConnections => {
            return res.json(socketConnections);
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    })

    app.delete('/', (req, res) => {
        let conditions = req.body;
        let socketConnections;
        SocketConnection.find(conditions)
        .then((a) => {
            socketConnections = a;
            return SocketConnection.remove(conditions)
        })
        .then(() => {
            return res.json(socketConnections);
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    })

    app.get('/:id', (req, res) => {
        SocketConnection.findById(req.params.id)
        .then((socketConnection) => {
            return res.json(socketConnection);
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    })

    app.patch('/:id', (req, res) => {
        const { id } = req.params;
        const changes = req.body;
        SocketConnection.findByIdAndUpdate(id, { $set: changes })
        .then(() => SocketConnection.findById(id))
        .then(socketConnection => {
            return res.json(socketConnection);
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    })

    app.delete('/:id', (req, res) => {
        const { id }  = req.params;
        let deletedSocketConnection;
        SocketConnection.findById(id)
        .then(socketConnection => {
            deletedSocketConnection = socketConnection;
            return SocketConnection.findByIdAndRemove(id)
        })
        .then(() => {
            return res.json(deletedSocketConnection);
        })
        .catch(err => {
            console.log(err);
            return res.status(400).send(err.message);
        })
    })



})
.catch(err => {
    console.log('mongodb error', err);
    process.exit(1);
})

const PORT = process.env.PORT || 4000;
server.listen(PORT)
console.log(`Magic happens on port ${PORT}`)       // shoutout to the user
console.log(`==== Running in ${process.env.NODE_ENV} mode ===`)
exports = module.exports = app             // expose app
